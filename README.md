
# mosaik-mqtt

This package contains a mqtt client adapter to publish data from mosaik simulation to a MQTT broker.

## Usage

To use the simulator, first add it to your `sim_config`:

```python
sim_config = {
      'MQTT': {'python': 'mosaik_mqtt:MosaikMQTT'},
    # ...
}
```

Next, include an `attribute_config.json` file in your project, specifying the topics to which you want to subscribe. 
It can be structured as follows:

```json
"test_attr": {
    "topic": "base_topic/greeter",
    "field_name": "test_attr"},
    #...

```
Note: `base_topic` and `field_name` are optional.

Now you can initiate the simulators: 

```python
mqtt_simulator = world.start('MQTT', attr_config='attribute_config.json',
                             step_size=1, encoder=json.dumps, decoder=json.loads)

```

After initialization, it's time to launch the MQTT model entity using your credentials. 
By default, the port is set to 1883, and you need to specify the host (i.e., the broker, for instance, the free HiveMQ broker). 
Optionally, you can provide a username/password or a base_topic like this:

```python
mqtt_entity = mqtt_simulator.MQTTClient(host="broker.hivemq.com", port=1883,
                                          username='Hello', password='World',
                                          base_topic="base_topic/#")
```
The next step is to add the data you wish to publish. 
Following that, you can define world.connect(publish_entity, mqtt_entity, attrs) as needed. 
Please note that the simulator supports only one instance of the MQTT Client. 
If you wish to connect to multiple MQTT Clients, you will need to initiate multiple instances of the simulator as well.