import mosaik
import json

sim_config = {
   'MQTT': {
      'python': 'mosaik_mqtt:MosaikMQTT'
   },
   'CSV': {
        'python': 'examples.simulator.csv_simulator:CSV',
    },
    'Collector': {
        #'cmd': 'python collector.py %(addr)s',
        'python': 'examples.simulator.collector:Collector'
    },
}

world = mosaik.World(sim_config)

mqtt_simulator = world.start('MQTT', attr_config='examples/data/attribute_config.json',
                             step_size=1, encoder=json.dumps, decoder=json.loads)
csv_sim = world.start('CSV', datafile='examples/data/csv_input.csv', step_size=1)
collector = world.start('Collector', step_size=1)

#mqtt_entity = mqtt_simulator.MQTTClient(host="mqtt.eclipseprojects.io", port=1883,
#                                        base_topic="test_mosaik")
mqtt_entity = mqtt_simulator.MQTTClient(host="broker.hivemq.com", port=1883,
                                        base_topic="OFFIS/test_mosaik/#")

csv_entity = csv_sim.CSV_Data()
monitor = collector.Monitor()

world.connect(csv_entity, mqtt_entity, 'example_schedule')
world.connect(csv_entity, monitor, 'example_schedule')
#world.connect(mqtt_entity, monitor, 'example_function', 'test_attr', 'b445_irrad')

world.run(until=3, rt_factor=1.)
