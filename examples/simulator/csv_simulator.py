import mosaik_api
import csv
import json


class CSV(mosaik_api.Simulator):
    def __init__(self):
        super().__init__({'models': {}})
        self.datafile = None
        self.next_row = None
        self.modelname = None
        self.step_size = None
        self.time_factor = None
        self.attrs = None
        self.eids = []
        self.cache = None
        self.meta['type'] = 'time-based'

    def init(self, sid, time_resolution, datafile, step_size=None, time_factor=1.):
        self.csvFile = open(datafile)
        self.csvReader = csv.reader(self.csvFile)
        self.modelname = next(self.csvReader)[0]
        print('MODEL NAME', self.modelname)
        self.step_size = step_size
        self.time_factor = time_factor

        # Get attribute names and strip optional comments
        attrs = next(self.csvReader)[1:]
        for i, attr in enumerate(attrs):
            try:
                # Try stripping comments
                attr = attr[:attr.index('#')]
            except ValueError:
                pass
            attrs[i] = attr.strip()
        self.attrs = attrs

        self.meta['models'][self.modelname] = {
            'public': True,
            'params': [],
            'attrs': attrs,
        }

        # Read first data row
        self._read_next_row()

        return self.meta

    def create(self, num, model):
        if model != self.modelname:
            raise ValueError('Invalid model "%s" % model')

        start_idx = len(self.eids)
        entities = []
        for i in range(num):
            eid = '%s_%s' % (model, i + start_idx)
            entities.append({
                'eid': eid,
                'type': model,
                'rel': [],
            })
            self.eids.append(eid)

        return entities

    def step(self, time, max_advance, inputs=None):
        data = self.next_row
        print('========', data)
        if data is None:
            raise IndexError('End of CSV file reached.')

        requested_time = round(time*self.time_factor, 5)  # Due to (binary) rounding errors, we have to round it
        while float(data[0]) < requested_time:
            self._read_next_row()
            data = self.next_row
            print('========', data)
            if data is None:
                break

        # Put data into the cache for get_data() calls
        self.cache = {}

        if data is None or float(data[0]) != requested_time:
            #print(float(data[0]), self.time_factor)
            #raise IndexError('Requested time "%s" not found.' % (
            #    requested_time))
            for attr in self.attrs:
                self.cache[attr] = None
        else:
            for attr, val in zip(self.attrs, data[1:]):
                #json.dumps(json.loads(str(val).replace("'", '"')))
                #val_str = str(val).replace("'", '"')
                #print(val_str, type(val_str))
                try:
                    val = json.loads(str(val).replace("'", '"'))
                except:
                    pass
                if val != '':
                    self.cache[attr] = val
                else:
                    self.cache[attr] = None
        print('CACHE', self.cache)
        if self.step_size:
            return time + self.step_size
        else:
            self._read_next_row()
            if self.next_row is not None:
                next_step = round(float(self.next_row[0]) / self.time_factor, 5)
                if not next_step.is_integer():
                    raise ValueError('Next value "%s" is not mappable to integer via time_factor of "%s", gives "%s".' % (self.next_row[0],  self.time_factor, next_step))
                return int(next_step)
            else:
                return (time + 1) * 1000000000

    def get_data(self, outputs):
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self.eids:
                raise ValueError('Unknown entity ID "%s"' % eid)

            data[eid] = {}
            for attr in attrs:
                data[eid][attr] = self.cache[attr]

        return data

    def _read_next_row(self):
        try:
            self.next_row = next(self.csvReader)#.strip().split(',')
        except StopIteration:
            self.next_row = None

    def finalize(self):
        self.csvFile.close()


def main():
    return mosaik_api.start_simulation(CSV(), 'mosaik-csv simulator')
