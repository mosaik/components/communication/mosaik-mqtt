"""
Publishes mosaik simulation data at a MQTT broker.

"""
import paho.mqtt.client as mqtt
import mosaik_api
import json
from datetime import datetime

__version__ = '0.1'

meta = {'type': 'time-based',
    'models':
            {'MQTTClient':
                 {'public': True,
                  'any_inputs': True,
                  'params': ['host', 'port', 'attr_config', 'base_topic', 'username', 'password'],
                  'attrs': [],
                  },
             },
        }


class MosaikMQTT(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(meta)
        self.attr_config = None
        self.attr_map = {}
        self.sid = None
        self.eid = None
        self.step_size = None
        self.mqttc = None
        self.encoder = None
        self.decoder = None
        self.base_topic = None
        self.data = {}
        self.time_resolution = None

    def init(self, sid, time_resolution, attr_config, step_size=1, encoder=None, decoder=None):

        with open(attr_config) as jfile:
            jstr = jfile.read()
            jdata = json.loads(jstr)
            self.attr_config = jdata

        for mosaik_attr, config_dict in self.attr_config.items():
            imap = config_dict.copy()
            topic = imap.pop('topic')
            imap['mosaik_attr'] = mosaik_attr
            self.attr_map[topic] = imap

        sim_attrs = self.meta['models']['MQTTClient']['attrs']
        for attr in self.attr_config.keys():
            if attr not in sim_attrs:
                sim_attrs.append(attr)
        self.sid = sid
        self.step_size = step_size
        self.encoder = encoder
        self.decoder = decoder
        return self.meta

    def create(self, num, model, host, port=1883, base_topic=None,
               username=None, password=None):
        if num != 1 or self.eid is not None:
            raise ValueError('Can only create one mqtt client per simulator.')
        if model != 'MQTTClient':
            raise ValueError('Unknown model: "%s"' % model)

        self.eid = 'mqtt_0'

        #if base_topic is not isinstance(base_topic, str):
        #    raise ValueError('MQTT client: base_topic needs to be string.')
        #self.base_topic = base_topic

        self.mqttc = mqtt.Client()
        self.mqttc.on_message = self._on_message
        self.mqttc.on_connect = self._on_connect
        self.mqttc.on_publish = self._on_publish
        self.mqttc.on_subscribe = self._on_subscribe
        # Uncomment to enable debug messages
        self.mqttc.on_log = self._on_log

        if username or password:
            assert username or password, "Error in MQTTClient: Username and" \
                                         "password have to be provided" \
                                         "together."
            self.mqttc.username_pw_set(username, password)
            import ssl
            self.mqttc.tls_set(ca_certs=None, certfile=None, keyfile=None,
                               cert_reqs=ssl.CERT_OPTIONAL,
                               tls_version=ssl.PROTOCOL_TLS, ciphers=None)

        self.mqttc.connect(host, port, 60)

        self.mqttc.loop_start()
        self.mqttc.subscribe(base_topic)

        return [{'eid': self.eid, 'type': model, 'rel': []}]

    def step(self, time, inputs, max_advance):
        # There's only one entity :
        inputs = inputs.get(self.eid, {})
        # payload = {ikey: list(ivalue.values())[0] for ikey, ivalue in
        # inputs.items()}
        payloads = {}
        for iattr, ivalue in inputs.items():
            assert len(ivalue.values()) == 1, f"Error in MQTTClient: " \
                                              f"Attribute {iattr} contains " \
                                              f"more than one value."
            val = list(ivalue.values())[0]

            if val is not None:
                topic = self.attr_config[iattr]['topic']
                field_name = self.attr_config[iattr].get("field_name", None)
                #payload = [{'fields': {field_name: val}}]
                if field_name:
                    payloads.setdefault(topic, {"data": {}})["data"][field_name] = val
                else:
                    payloads[topic] = val

        for topic, payload in payloads.items():
            #print('UNENCODED', payload)
            #payload['time'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:23]
            if self.encoder:
                payload = self.encoder(payload)
            print('PUBLISH', payload)
            infot = self.mqttc.publish(topic, payload, qos=2, retain=True)

            infot.wait_for_publish()

        return time + self.step_size

    def get_data(self, outputs):
        data = {}
        for eid, mosaik_attrs in outputs.items():
            data[eid] = {}
            for mosaik_attr in mosaik_attrs:
                topic = self.attr_config[mosaik_attr]['topic']
                field_name = self.attr_config[mosaik_attr]['field_name']
                data[eid][mosaik_attr] = self.data.get(topic, {}).get('fields', {}).get(field_name, None)
        return data

    def finalize(self):
        print('DATA:', self.data)
        self.mqttc.loop_stop()
        self.mqttc.disconnect()

    def _on_connect(self, mqttc, obj, flags, rc):
        print("Connect: rc: " + str(rc))

    def _on_message(self, mqttc, obj, msg):
        print("Message", msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
        if self.decoder:
            payload = self.decoder(msg.payload)
        else:
            payload = msg.payload
        #assert len(payload) == 1
        if isinstance(payload, list):
            payload = payload[0]
        if 'time' in payload:
            #print('TIME', float(time_epoch()) - float(payload.pop('time')))
            print('TIME', payload.pop('time'))
        self.data[msg.topic] = payload

    def _on_publish(self, mqttc, obj, mid):
        print("Publish: mid: " + str(mid))
        pass

    def _on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: " + str(mid) + " " + str(granted_qos))

    def _on_log(self, mqttc, obj, level, string):
        print("Log:", string)


def main():
    desc = __doc__.strip()
    mosaik_api.start_simulation(MosaikMQTT(), desc)
