from setuptools import setup


setup(
    name='mosaik-mqtt',
    version='0.1',
    author='Thomas Raub',
    author_email='mosaik@offis.de',
    description=('Publishes mosaik simulation data at a MQTT broker.'),
    long_description=(open('README.md').read() + '\n\n' +
                      open('AUTHORS.txt').read()),
    url='https://gitlab.com/mosaik/internal/mosaik-mqtt',
    install_requires=[
        'mosaik>=3.0.0',
        'mosaik-api>=3.0.0',
        'mosaik-csv>=1.2.0',
        'paho-mqtt>=1.5.1',
    ],
    include_package_data=True,

    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: MIT',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Topic :: Scientific/Engineering',
    ],
)
